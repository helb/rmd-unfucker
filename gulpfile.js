const gulp = require("gulp");
const dom = require("gulp-dom");
const crisper = require("gulp-crisper");
const postcss = require("gulp-postcss");
const cssnano = require("cssnano");
const terser = require("gulp-terser");
const pipeline = require("readable-stream").pipeline;
const stripFontFace = require("postcss-strip-font-face");
const fs = require("fs");
const htmlmin = require("gulp-htmlmin");
const through = require("through2");

const DIR = process.env.DIR;
const INPUT = `${DIR}/index.html`;

const noop = () => through.obj();

gulp.task("extract-js", function() {
  return gulp
    .src(INPUT)
    .pipe(
      process.env.OPTIMIZE_JS ? crisper({
        scriptInHead: true,
        onlySplit: false
      }) : noop()
    )
    .pipe(gulp.dest(DIR));
});

gulp.task("extract-css", function() {
  return gulp
    .src(INPUT)
    .pipe(
      dom(function() {
        const styles = this.querySelectorAll("style[type='text/css']");
        output = "";
        const filename = "index.css";
        styles.forEach((style, i) => {
          output += style.innerHTML;
          style.parentNode.removeChild(style);
        });
        const link = this.createElement("link");
        link.setAttribute("href", filename);
        link.setAttribute("rel", "stylesheet");
        this.head.appendChild(link);
        fs.writeFileSync(`${DIR}/${filename}`, output);

        return;
      }, false)
    )
    .pipe(gulp.dest(DIR));
});

gulp.task("extract-images", function() {
  return gulp
    .src(INPUT)
    .pipe(
      dom(function() {
        const images = this.querySelectorAll(
          "img[src^='data:image/png;base64']"
        );
        images.forEach((image, i) => {
          const filename = i + ".png";
          const imgData = new Buffer.from(image.src.split(",")[1], "base64");
          image.setAttribute("src", filename);
          fs.writeFileSync(`${DIR}/${filename}`, imgData);
        });

        return;
      }, false)
    )
    .pipe(gulp.dest(DIR));
});

gulp.task("optimize-css", () => {
  var plugins = [stripFontFace(), cssnano()];
  return gulp
    .src(`${DIR}/*.css`)
    .pipe(postcss(plugins))
    .pipe(gulp.dest(DIR));
});

gulp.task("optimize-js", function() {
  return pipeline(
    gulp.src(`${DIR}/*.js`),
    process.env.OPTIMIZE_JS ? terser() : noop(),
    gulp.dest(DIR)
  );
});

gulp.task("move-og-tags", function() {
  return gulp
    .src(INPUT)
    .pipe(
      dom(function() {
        const tags = this.querySelectorAll("head meta[property^='og:'], head meta[property^='twitter:']");
        const title = this.querySelector("head title");
        const doc = this;
        [...tags].reverse().forEach(tag => {
          doc.head.insertBefore(tag, title.nextSibling);
        });
        return;
      }, false)
    )
    .pipe(gulp.dest(DIR));
});

gulp.task("add-preload-links", function() {
  return gulp
    .src(INPUT)
    .pipe(
      dom(function() {
        const jspreload = this.createElement("link");
        jspreload.setAttribute("rel", "preload");
        jspreload.setAttribute("as", "script");
        jspreload.setAttribute("href", "index.js");
        this.head.insertBefore(
          jspreload,
          this.head.querySelector(":first-child")
        );
        const csspreload = this.createElement("link");
        csspreload.setAttribute("rel", "preload");
        csspreload.setAttribute("as", "style");
        csspreload.setAttribute("href", "index.css");
        this.head.insertBefore(csspreload, jspreload);
        return;
      }, false)
    )
    .pipe(gulp.dest(DIR));
});

gulp.task("htmlmin", () => {
  return gulp
    .src(INPUT)
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(DIR));
});

gulp.task(
  "default",
  gulp.series(
    "extract-js",
    "extract-css",
    "extract-images",
    "optimize-css",
    "optimize-js",
    "move-og-tags",
    "htmlmin"
  )
);
